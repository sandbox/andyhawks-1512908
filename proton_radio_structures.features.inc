<?php
/**
 * @file
 * proton_radio_structures.features.inc
 */

/**
 * Implements hook_views_api().
 */
function proton_radio_structures_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function proton_radio_structures_node_info() {
  $items = array(
    'artist' => array(
      'name' => t('Artist'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'episode' => array(
      'name' => t('Episode'),
      'base' => 'node_content',
      'description' => t('Use for an individual episode of a Show. '),
      'has_title' => '1',
      'title_label' => t('Episode Title'),
      'help' => '',
    ),
    'label' => array(
      'name' => t('Label'),
      'base' => 'node_content',
      'description' => t('Record labels on protonradio.com'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'mix' => array(
      'name' => t('Mix'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'playlist_item' => array(
      'name' => t('Playlist Item'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'release' => array(
      'name' => t('Release'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'show' => array(
      'name' => t('Show'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  return $items;
}
