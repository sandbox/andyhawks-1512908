<?php
/**
 * @file
 * proton_structures.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function proton_structures_user_default_roles() {
  $roles = array();

  // Exported role: artist
  $roles['artist'] = array(
    'name' => 'artist',
    'weight' => '3',
  );

  // Exported role: label assistant
  $roles['label assistant'] = array(
    'name' => 'label assistant',
    'weight' => '6',
  );

  // Exported role: label owner
  $roles['label owner'] = array(
    'name' => 'label owner',
    'weight' => '5',
  );

  // Exported role: show owner
  $roles['show owner'] = array(
    'name' => 'show owner',
    'weight' => '4',
  );

  return $roles;
}
