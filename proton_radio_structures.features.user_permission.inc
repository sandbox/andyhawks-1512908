<?php
/**
 * @file
 * proton_radio_structures.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function proton_radio_structures_user_default_permissions() {
  $permissions = array();

  // Exported permission: create artist content
  $permissions['create artist content'] = array(
    'name' => 'create artist content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: create episode content
  $permissions['create episode content'] = array(
    'name' => 'create episode content',
    'roles' => array(
      0 => 'administrator',
      1 => 'show owner',
    ),
    'module' => 'node',
  );

  // Exported permission: create field_id_phpbb2
  $permissions['create field_id_phpbb2'] = array(
    'name' => 'create field_id_phpbb2',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_id_proton
  $permissions['create field_id_proton'] = array(
    'name' => 'create field_id_proton',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_label_directory
  $permissions['create field_label_directory'] = array(
    'name' => 'create field_label_directory',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_name_first
  $permissions['create field_name_first'] = array(
    'name' => 'create field_name_first',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
      2 => 'label assistant',
      3 => 'label owner',
      4 => 'show owner',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_name_last
  $permissions['create field_name_last'] = array(
    'name' => 'create field_name_last',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
      2 => 'label assistant',
      3 => 'label owner',
      4 => 'show owner',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create label content
  $permissions['create label content'] = array(
    'name' => 'create label content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: create mix content
  $permissions['create mix content'] = array(
    'name' => 'create mix content',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
      2 => 'show owner',
    ),
    'module' => 'node',
  );

  // Exported permission: create playlist_item content
  $permissions['create playlist_item content'] = array(
    'name' => 'create playlist_item content',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'node',
  );

  // Exported permission: create release content
  $permissions['create release content'] = array(
    'name' => 'create release content',
    'roles' => array(
      0 => 'administrator',
      1 => 'label assistant',
      2 => 'label owner',
    ),
    'module' => 'node',
  );

  // Exported permission: create show content
  $permissions['create show content'] = array(
    'name' => 'create show content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any artist content
  $permissions['delete any artist content'] = array(
    'name' => 'delete any artist content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any episode content
  $permissions['delete any episode content'] = array(
    'name' => 'delete any episode content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any label content
  $permissions['delete any label content'] = array(
    'name' => 'delete any label content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any mix content
  $permissions['delete any mix content'] = array(
    'name' => 'delete any mix content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any playlist_item content
  $permissions['delete any playlist_item content'] = array(
    'name' => 'delete any playlist_item content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any release content
  $permissions['delete any release content'] = array(
    'name' => 'delete any release content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any show content
  $permissions['delete any show content'] = array(
    'name' => 'delete any show content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own artist content
  $permissions['delete own artist content'] = array(
    'name' => 'delete own artist content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own episode content
  $permissions['delete own episode content'] = array(
    'name' => 'delete own episode content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own label content
  $permissions['delete own label content'] = array(
    'name' => 'delete own label content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own mix content
  $permissions['delete own mix content'] = array(
    'name' => 'delete own mix content',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
      2 => 'show owner',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own playlist_item content
  $permissions['delete own playlist_item content'] = array(
    'name' => 'delete own playlist_item content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own release content
  $permissions['delete own release content'] = array(
    'name' => 'delete own release content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own show content
  $permissions['delete own show content'] = array(
    'name' => 'delete own show content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any artist content
  $permissions['edit any artist content'] = array(
    'name' => 'edit any artist content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any episode content
  $permissions['edit any episode content'] = array(
    'name' => 'edit any episode content',
    'roles' => array(
      0 => 'administrator',
      1 => 'show owner',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any label content
  $permissions['edit any label content'] = array(
    'name' => 'edit any label content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any mix content
  $permissions['edit any mix content'] = array(
    'name' => 'edit any mix content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any playlist_item content
  $permissions['edit any playlist_item content'] = array(
    'name' => 'edit any playlist_item content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any release content
  $permissions['edit any release content'] = array(
    'name' => 'edit any release content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any show content
  $permissions['edit any show content'] = array(
    'name' => 'edit any show content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit field_id_phpbb2
  $permissions['edit field_id_phpbb2'] = array(
    'name' => 'edit field_id_phpbb2',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_id_proton
  $permissions['edit field_id_proton'] = array(
    'name' => 'edit field_id_proton',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_label_directory
  $permissions['edit field_label_directory'] = array(
    'name' => 'edit field_label_directory',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_name_first
  $permissions['edit field_name_first'] = array(
    'name' => 'edit field_name_first',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_name_last
  $permissions['edit field_name_last'] = array(
    'name' => 'edit field_name_last',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own artist content
  $permissions['edit own artist content'] = array(
    'name' => 'edit own artist content',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own episode content
  $permissions['edit own episode content'] = array(
    'name' => 'edit own episode content',
    'roles' => array(
      0 => 'administrator',
      1 => 'show owner',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own field_id_phpbb2
  $permissions['edit own field_id_phpbb2'] = array(
    'name' => 'edit own field_id_phpbb2',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_id_proton
  $permissions['edit own field_id_proton'] = array(
    'name' => 'edit own field_id_proton',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_label_directory
  $permissions['edit own field_label_directory'] = array(
    'name' => 'edit own field_label_directory',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_name_first
  $permissions['edit own field_name_first'] = array(
    'name' => 'edit own field_name_first',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
      2 => 'label assistant',
      3 => 'label owner',
      4 => 'show owner',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_name_last
  $permissions['edit own field_name_last'] = array(
    'name' => 'edit own field_name_last',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
      2 => 'label assistant',
      3 => 'label owner',
      4 => 'show owner',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own label content
  $permissions['edit own label content'] = array(
    'name' => 'edit own label content',
    'roles' => array(
      0 => 'administrator',
      1 => 'label assistant',
      2 => 'label owner',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own mix content
  $permissions['edit own mix content'] = array(
    'name' => 'edit own mix content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own playlist_item content
  $permissions['edit own playlist_item content'] = array(
    'name' => 'edit own playlist_item content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own release content
  $permissions['edit own release content'] = array(
    'name' => 'edit own release content',
    'roles' => array(
      0 => 'administrator',
      1 => 'label assistant',
      2 => 'label owner',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own show content
  $permissions['edit own show content'] = array(
    'name' => 'edit own show content',
    'roles' => array(
      0 => 'administrator',
      1 => 'show owner',
    ),
    'module' => 'node',
  );

  // Exported permission: view field_id_phpbb2
  $permissions['view field_id_phpbb2'] = array(
    'name' => 'view field_id_phpbb2',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_id_proton
  $permissions['view field_id_proton'] = array(
    'name' => 'view field_id_proton',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_label_directory
  $permissions['view field_label_directory'] = array(
    'name' => 'view field_label_directory',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_name_first
  $permissions['view field_name_first'] = array(
    'name' => 'view field_name_first',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_name_last
  $permissions['view field_name_last'] = array(
    'name' => 'view field_name_last',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_id_phpbb2
  $permissions['view own field_id_phpbb2'] = array(
    'name' => 'view own field_id_phpbb2',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_id_proton
  $permissions['view own field_id_proton'] = array(
    'name' => 'view own field_id_proton',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_label_directory
  $permissions['view own field_label_directory'] = array(
    'name' => 'view own field_label_directory',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_name_first
  $permissions['view own field_name_first'] = array(
    'name' => 'view own field_name_first',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
      2 => 'label assistant',
      3 => 'label owner',
      4 => 'show owner',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_name_last
  $permissions['view own field_name_last'] = array(
    'name' => 'view own field_name_last',
    'roles' => array(
      0 => 'administrator',
      1 => 'artist',
      2 => 'label assistant',
      3 => 'label owner',
      4 => 'show owner',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
